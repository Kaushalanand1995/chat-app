const socket = io();

document.querySelector('#message-form').addEventListener('submit', (e) => {
    e.preventDefault();

    const message = document.querySelector('input').value;

    socket.emit('sendMessage', message);
})